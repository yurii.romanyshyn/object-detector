install:
	python3 -m venv .venv
	source .venv
	pip install -r requirements.txt
	pip install -e .

start_web:
	export FLASK_ENV=development
	export FLASK_DEBUG=1
	flask run

start_worker:
	flask worker -v 