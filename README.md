### Installation

In order to install project virtual env and dependencies please run `make install`

To run flask app - `make start_web` or `flask run`

To run broker app - `make start_worker` or `flask worker -p [processes number] -t [threads number] -v`

Also, the application requires MongoDB instance as a database store and 
Redis instance as the queue for tasks broker. Connectivity configuration 
for them`s you can set-up in the settings.py

*Alternatively*, you can use the `docker-compose up` command to set-up the application environment and start it.

-------
### System Components and design

![alt text](img/system-components.jpg "System Components")

General describing-schema of the application

- Flask APP - basic flask application
- Worker N - dramatiq worker
- Tasks queue - redis instance
- Database - mongodb instance

-------
### Video processing workflow

![alt text](img/video-processing-workflow.jpg "System Components")

Steps produced by the application in order to detect objects in the video 
