import os
from pathlib import Path
parent_dir = Path(__file__).parent / "challenge" / "settings.py"
os.environ.setdefault("FLASK_SETTINGS", str(parent_dir.absolute()))
from challenge import create_app
app = create_app()