

def chunk(list_, chunk_size):
    for index in range(0, len(list_), chunk_size):
        yield list_[index:index + chunk_size]