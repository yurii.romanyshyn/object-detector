from flask import Flask
from flask_uploads import configure_uploads
from flask_uploads import patch_request_class
from dramatiq.results.backends import RedisBackend
from dramatiq.results import Results
from dramatiq import set_broker
from .extensions import dramatiq
from .extensions import video_storage
from .extensions import frame_storage
from .extensions import mongo
from .tasks import * # without this import 'actors' will be not attached to the dramatiq object



def create_app():
    app = Flask("challenge")
    app.config.from_envvar("FLASK_SETTINGS")
    register_extensions(app)
    register_blueprints(app)
    return app


def register_extensions(app):
    configure_uploads(app, (video_storage, frame_storage))
    patch_request_class(app, 3 * 1024 * 1024)
    dramatiq.middleware.append(Results(backend=RedisBackend()))
    dramatiq.init_app(app)
    set_broker(dramatiq.broker)
    mongo.init_app(app)


def register_blueprints(app):
    from .views import blueprint
    from flask_uploads import uploads_mod
    app.register_blueprint(blueprint)
    app.register_blueprint(uploads_mod)