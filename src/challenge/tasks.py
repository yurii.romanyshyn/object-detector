import cv2
import logging
from pathlib import Path
from threading import local
from .predictor import Predictor
from .extensions import dramatiq
from .extensions import video_storage
from .extensions import frame_storage
from .documents import Task
from .utils import chunk


logger = logging.getLogger("challenge.tasks")
local_storage = local()


def get_predictor():
    if not hasattr(local_storage, "predictor"):
        local_storage.predictor = Predictor(
            model_config_file=dramatiq.app.config["MODEL_CONFIG_FILE"],
            options=dramatiq.app.config["MODEL_OPTIONS"],
            score_thresh=dramatiq.app.config["MODEL_SCORE_THRESH"]
        )
    return local_storage.predictor


@dramatiq.actor(max_retries=0, max_age=3_600_000, time_limit=900_000)
def chunk_video_into_frames(task_id):
    task = Task.objects(id=task_id).first()
    if not task:
        return
    
    filepath = Path(video_storage.path(task.video_file))
    video_reader = cv2.VideoCapture(str(filepath.absolute()))
    video_reader_fps = dramatiq.app.config.get("CV2_FPS")
    
    if video_reader_fps is not None:
        video_reader.set(cv2.CAP_PROP_FPS, video_reader_fps)
    
    success, image = video_reader.read()
    frame_index = 0
    frames = []
    
    while success:
        frame_filename = f"{task_id}-frame-{frame_index}.jpg"
        filepath = frame_storage.path(frame_filename)
        cv2.imwrite(filepath, image)
        success, image = video_reader.read()
        frames.append(frame_filename)
        frame_index += 1

    frame_chunk_size = dramatiq.app.config.get("FRAME_CHUNK_SIZE", 30)
    frames_chunks = list(chunk(frames, frame_chunk_size))
    
    task.frames_count = len(frames)
    task.frames_chunks = frames_chunks
    task.save()
    
    for chunk_key in range(0, len(frames_chunks)):
        detect_objects_on_frames_chunk.send(chunk_key, task_id)



@dramatiq.actor(max_retries=0, max_age=3_600_000, time_limit=900_000)
def detect_objects_on_frames_chunk(chunk_key, task_id):
    task = Task.objects(id=task_id).first()
    if not task:
        return

    failed_frames = []
    successful_frames = []
    collected_data = []
    frames_chunk = task.frames_chunks[chunk_key]
    
    try:
        while frames_chunk:
            frame_filename = frames_chunk.pop()
            filepath = Path(frame_storage.path(frame_filename))
            image = cv2.imread(str(filepath.absolute()))
            
            if image is None:
                failed_frames.append(frame_filename)
                continue
            
            predictor = get_predictor()
            data, visualized_image = predictor.detect_objects(image, visualize=task.with_objects_visualization)

            if visualized_image:
                visualized_image.save(str(filepath.absolute()))
            
            successful_frames.append(frame_filename)
            collected_data.append({
                "filename": frame_filename,
                "data": data
            })
    
    except Exception:
        logger.exception(f"Object detection on chunk of frames ({chunk_key}) failed!")
        failed_frames.extend(frames_chunk)
    
    finally:
        task.update(push_all__successful_frames=successful_frames)
        task.update(push_all__failed_frames=failed_frames)
        task.update(push_all__data=collected_data)