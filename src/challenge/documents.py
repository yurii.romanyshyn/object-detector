from datetime import datetime
from mongoengine import DynamicDocument
from mongoengine import StringField
from mongoengine import ListField
from mongoengine import DictField
from mongoengine import DateTimeField
from mongoengine import IntField
from mongoengine import BooleanField



class Task(DynamicDocument):
    video_file = StringField()
    with_objects_visualization = BooleanField(default=False)
    frames_count = IntField()
    frames_chunks = ListField(ListField())
    successful_frames = ListField(StringField())
    failed_frames = ListField(StringField())
    data = ListField(DictField())
    created_at = DateTimeField(default=datetime.now)