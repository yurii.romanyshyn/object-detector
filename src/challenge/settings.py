import os
from pathlib import Path

ENV = "development"
DEBUG = True
SECRET_KEY = "123"

PROJECT_ROOT_DIR = Path(__file__).parent.parent.parent
MEDIA_DIR = os.environ.get("MEDIA_DIR")

if not MEDIA_DIR:
    MEDIA_DIR = PROJECT_ROOT_DIR / "media"
    if not MEDIA_DIR.exists(): MEDIA_DIR.mkdir()
else:
    MEDIA_DIR = Path(MEDIA_DIR)


DRAMATIQ_BROKER = "dramatiq.brokers.redis:RedisBroker"
DRAMATIQ_BROKER_URL = os.environ.get("DRAMATIQ_BROKER_URL")


MODEL_CONFIG_FILE = "COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"
MODEL_OPTIONS = ["MODEL.DEVICE", "cpu"]
MODEL_SCORE_THRESH = 0.5
CV2_FPS = 10 # Frame rate


MONGODB_DB = os.environ.get("MONGODB_HOST", "objects_detector")
MONGODB_HOST = os.environ.get("MONGODB_HOST")
MONGODB_PORT = os.environ.get("MONGODB_PORT")
# MONGODB_USERNAME = None
# MONGODB_PASSWORD = None