import torch
import numpy
from functools import partial
from typing import List
from typing import Dict
from typing import Union
from typing import Tuple
from typing import Optional
from detectron2 import model_zoo
from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog
from detectron2.engine import DefaultPredictor
from detectron2.structures.instances import Instances
from detectron2.utils.visualizer import VisImage
from detectron2.utils.visualizer import Visualizer


ObjectPrediction = Dict[Union[int, str], float]  # type alias; dict[ obj_class_id | obj_class_name, prediction %]


class Predictor:

    def __init__(
        self, 
        model_config_file: str, 
        options: List[str],
        score_thresh: float
    ):
        self.config = get_cfg()
        self.config.merge_from_file(model_zoo.get_config_file(model_config_file))
        self.config.merge_from_list(options)
        self.config.MODEL.ROI_HEADS.SCORE_THRESH_TEST = score_thresh
        self.config.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(model_config_file)
        self.metadata = MetadataCatalog.get(self.config.DATASETS.TEST[0])
        self.predictor = DefaultPredictor(self.config)

    def detect_objects(self, image: numpy.ndarray, visualize: bool = False) -> Tuple[ObjectPrediction, Optional[VisImage]]:
        output = self.predictor(image)
        instances = output["instances"].to(torch.device("cpu"))
        result = self.prepare_output_data(instances)
        if visualize:
            visualized_image = self.visualize(image, instances)
            return result, visualized_image
        else:
            return result, None
    
    def prepare_output_data(self, predictions: Instances) -> ObjectPrediction:
        scores = getattr(predictions, "scores", None)
        classes = getattr(predictions, "pred_classes", None)

        if scores is None or classes is None:
            return {}
        
        thing_classes = self.metadata.get("thing_classes")
        round_score = partial(round, ndigits=4)
        scores_iter = map(round_score, map(float, scores))
        
        if not thing_classes:
            return dict(zip(classes, scores_iter))
        else:
            classes_names = [thing_classes[i] for i in classes]
            return dict(zip(classes_names, scores_iter))
    
    def visualize(self, image: numpy.ndarray, predictions: Instances) -> VisImage:
        visualizer = Visualizer(image, self.metadata)
        return visualizer.draw_instance_predictions(predictions=predictions)
