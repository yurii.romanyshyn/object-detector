from flask import Blueprint
from flask import request
from flask import redirect
from flask import url_for
from flask import abort
from flask import render_template
from .tasks import chunk_video_into_frames
from .extensions import video_storage
from .documents import Task


blueprint = Blueprint("views", __name__)


@blueprint.route("/task/<task_id>", methods=["GET"])
def task_details(task_id):
    counter = 10
    
    try:
        frames_count = int(request.args.get("frames_count", counter))
    except ValueError:
        frames_count = counter
    
    task = Task.objects(id=task_id).first()
    frames = task.data[:frames_count]
    
    if not task:
        abort(404)
    
    return render_template(
        "task-details.html", 
        task=task, 
        frames=frames,
        frames_count=frames_count,
        counter=counter
    )


@blueprint.route("/", methods=["GET"])
@blueprint.route("/tasks", methods=["GET"])
def tasks():
    tasks = Task.objects.all()
    return render_template("tasks-list.html", tasks=tasks)


@blueprint.route("/create-task", methods=["POST"])
def create_task():
    filename = video_storage.save(request.files['file'])
    task = Task(
        video_file=filename,
        with_objects_visualization=request.values.get("with_visualization", False)
    ).save()
    chunk_video_into_frames.send(str(task.id))
    return redirect(url_for('views.tasks'))