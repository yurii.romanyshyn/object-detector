from functools import partial
from flask_dramatiq import Dramatiq
from flask_uploads import UploadSet
from flask_mongoengine import MongoEngine


dramatiq = Dramatiq()
mongo = MongoEngine()


def get_storage_dest(folder_name, app):
    media_dir = app.config["MEDIA_DIR"]
    uploadset_dir = media_dir / folder_name
    if not uploadset_dir.exists():
        uploadset_dir.mkdir()
    return str((uploadset_dir).absolute())


video_storage = UploadSet(
    'video', 
    ["mp4"],
    default_dest=partial(get_storage_dest, "video")
)
frame_storage = UploadSet(
    'frames', 
    ["mp4"], 
    default_dest=partial(get_storage_dest, "frames")
)