FROM ubuntu

WORKDIR /app/
COPY . .

RUN apt update -y && \
    apt install -y ffmpeg libsm6 libxext6 && \
    apt install -y git && \
    apt install -y python3.8 && \
    apt install -y python3-pip && \
    pip3 install -r requirements.txt && \
    pip3 install 'git+https://github.com/facebookresearch/detectron2.git' && \
    pip3 install -e .

CMD echo ""